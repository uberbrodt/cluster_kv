defmodule ClusterKv.MixProject do
  use Mix.Project

  def project do
    [
      app: :cluster_kv,
      version: "0.1.0",
      elixir: "~> 1.9",
      start_permanent: Mix.env() == :prod,
      aliases: aliases(),
      deps: deps()
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger]
    ]
  end

  defp deps do
    [
      {:credo, "~> 1.2", only: [:test], runtime: false},
      {:dialyxir, "~> 0.5.1", only: [:dev, :test], runtime: false},
      {:libcluster, "~> 3.2"},
      {:libring, "~> 1.4"},
      {:poolboy, "~> 1.5"},
      {:states_language, "~> 0.2"}
    ]
  end

  defp aliases do
    [
      all_tests: [
        "compile --force --warnings-as-errors",
        "credo --strict",
        "format --check-formatted",
        "test",
        "dialyzer --halt-exit-status"
      ]
    ]
  end
end
