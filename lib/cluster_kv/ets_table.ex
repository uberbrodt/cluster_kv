defmodule ClusterKV.ETSTable do
  @moduledoc false
  use GenServer
  @db_options [:set, :public, :named_table, {:read_concurrency, true}, {:write_concurrency, true}]

  @spec create_table(name :: module()) :: :ets.tab()
  def create_table(name) do
    :ets.new(name, @db_options)
  end

  def start_link(name: name) do
    GenServer.start_link(__MODULE__, name)
  end

  def init(name) do
    create_table(name)
    {:ok, %{}}
  end
end
