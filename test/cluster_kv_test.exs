defmodule ClusterKVTest do
  use ExUnit.Case
  doctest ClusterKV

  @topologies [
    test: [
      strategy: Cluster.Strategy.Epmd,
      config: [hosts: []]
    ]
  ]

  @keyspace "com.myrealm"

  setup_all do
    ClusterKV.start_link(name: TestCluster, topologies: @topologies, replicas: 1, quorum: 1)
    [db: TestCluster]
  end

  test "test", %{db: db} do
    me = {self(), self()}
    ClusterKV.put(db, @keyspace, "test1:test", :hello)
    ClusterKV.put(db, @keyspace, "test1:test", :cruel)
    ClusterKV.put(db, @keyspace, "test1:test", :world)

    assert "test:1" = ClusterKV.get_wildcard_key(".test.light..status", ".", ":", "")

    ClusterKV.put_wildcard(
      db,
      @keyspace,
      "com.test..temp",
      {1_003_039_494, me},
      ".",
      ":",
      ""
    )

    assert {"test1:test", [:world, :cruel, :hello]} = ClusterKV.get(db, @keyspace, "test1:test")

    assert [{["com", "test", "", "temp"], {1_003_039_494, me}}] =
             ClusterKV.wildcard(
               db,
               @keyspace,
               "com.test.data.temp",
               ".",
               ":",
               ""
             )

    ClusterKV.update(db, @keyspace, "test1:test", :cruel, :remove)

    assert {"test1:test", [:world, :hello]} = ClusterKV.get(db, @keyspace, "test1:test")

    ClusterKV.update(db, @keyspace, "test1:test", :hi, :replace)

    assert {"test1:test", [:hi]} = ClusterKV.get(db, @keyspace, "test1:test")

    ClusterKV.update(db, @keyspace, "test1:test", [:hello, :beautiful, :world], :replace_list)

    assert {"test1:test", [:hello, :beautiful, :world]} =
             ClusterKV.get(db, @keyspace, "test1:test")

    ClusterKV.update(db, @keyspace, "test1:test", :beautiful, :last_30)

    assert {"test1:test", [:beautiful, :hello, :beautiful, :world]} =
             ClusterKV.get(db, @keyspace, "test1:test")

    ClusterKV.update(db, @keyspace, "test1:test", :beautiful, :unique)

    assert {"test1:test", [:beautiful, :hello, :world]} =
             ClusterKV.get(db, @keyspace, "test1:test")

    ClusterKV.put(db, @keyspace, "test1:round-robin", 0)

    assert {"test1:round-robin", [0]} = ClusterKV.get(db, @keyspace, "test1:round-robin")

    ClusterKV.update(db, @keyspace, "test1:round-robin", 3, :round_robin)

    assert {"test1:round-robin", [1]} = ClusterKV.get(db, @keyspace, "test1:round-robin")

    ClusterKV.update(db, @keyspace, "test1:round-robin", 3, :round_robin)

    assert {"test1:round-robin", [2]} = ClusterKV.get(db, @keyspace, "test1:round-robin")

    ClusterKV.update(db, @keyspace, "test1:round-robin", 3, :round_robin)

    assert {"test1:round-robin", [0]} = ClusterKV.get(db, @keyspace, "test1:round-robin")
  end

  test "update_if", %{db: db} do
    assert :updated = ClusterKV.update_if(db, @keyspace, "test.profile", :initial_value, :diff)

    assert :noop = ClusterKV.update_if(db, @keyspace, "test.profile", :initial_value, :diff)

    assert :updated = ClusterKV.update_if(db, @keyspace, "test.profile", :new_value, :diff)
    assert :noop = ClusterKV.update_if(db, @keyspace, "test.profile", :new_value, :diff)
    assert :noop = ClusterKV.update_if(db, @keyspace, "test.profile", :new_value, :diff)
  end
end
